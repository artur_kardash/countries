-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 12 2016 г., 03:21
-- Версия сервера: 5.5.44-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `count`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cities_languages`
--

CREATE TABLE IF NOT EXISTS `cities_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` varchar(20) NOT NULL,
  `language_id` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `cities_languages`
--

INSERT INTO `cities_languages` (`id`, `city_id`, `language_id`) VALUES
(1, '1', '1'),
(2, '1', '2'),
(3, '3', '3'),
(4, '4', '3'),
(5, '5', '2'),
(6, '6', '2'),
(7, '7', '1'),
(8, '10', '5'),
(9, '2', '3');

-- --------------------------------------------------------

--
-- Структура таблицы `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `id_count` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `city`
--

INSERT INTO `city` (`id`, `title`, `id_count`) VALUES
(1, 'Киев', '1'),
(2, 'Львов', '1'),
(3, 'Запорожье', '1'),
(4, 'Харьков', '1'),
(5, 'Москва', '2'),
(6, 'Санкт-Петербург', '2'),
(7, 'Петропавловск-Камчатский', '2'),
(8, 'Магадан', '2'),
(10, 'Торонто', '8');

-- --------------------------------------------------------

--
-- Структура таблицы `count`
--

CREATE TABLE IF NOT EXISTS `count` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `count`
--

INSERT INTO `count` (`id`, `title`) VALUES
(1, 'Украина'),
(2, 'Россия'),
(8, 'Канада');

-- --------------------------------------------------------

--
-- Структура таблицы `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `languages`
--

INSERT INTO `languages` (`id`, `name`) VALUES
(1, 'Украинский'),
(2, 'Русский'),
(3, 'Английский'),
(5, 'Французский');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

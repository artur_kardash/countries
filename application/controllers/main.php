<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('count_model');
	}
	
	public function index()
	{
		$this->home();
	}

	public function home()
	{
		$data['title'] = "Главная";
		$count_data['counts'] = $this->count_model->get_count();
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('table', $count_data);
		$this->load->view('footer');
	}

	public function city()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$id=intval($_POST['id']);
			$city_data['cit'] = $this->count_model->get_city($id);
			$city_html=$this->load->view('city', $city_data, TRUE);
			exit(json_encode($city_html));
		}
	}
	
	public function lang()
	{
		if(isset($_POST['id']) && !empty($_POST['id']))
		{
			$id=intval($_POST['id']);
			$lang_data['lan'] = $this->count_model->get_lang($id);
			$lang_html=$this->load->view('language', $lang_data, TRUE);
			exit(json_encode($lang_html));
		}
	}

	public function countries()
	{
	  	$data['title'] = "Страны";
		$count['count'] = $this->count_model->get_count();
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('countries', $count);
		$this->load->view('footer');
	}

	public function add()
	{
	  	$data['title'] = "Добавить страну";
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('add_count');
		$this->load->view('footer');
	}

	public function add_db()
	{
	     $add_count = $this->input->post('count');
	     $this->count_model->create($add_count);
	   	 $this->countries();
	}

	public function delete($id)
	{
		$this->count_model->delete($id);
		$this->countries();
	}

	public function edit($id)
	{
		$data['title'] = 'Редактирование';
		$count_data['count'] = $this->count_model->get_counts($id);
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('edit', $count_data);
		$this->load->view('footer');
	}

	public function update()
	{
		$this->load->model('count_model');
		$data=$this->input->post('count');
		$id=$this->input->post('id');
		$this->count_model->update($id, $data);
		header("Location: /main/countries");
	}

	public function cities()
	{
		$data['title'] = "Города";
		$cities['city'] = $this->count_model->get_cities();
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('cities', $cities);
		$this->load->view('footer');
	}

	public function add_city()
	{
	  	$data['title'] = "Добавить страну";
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('add_city');
		$this->load->view('footer');
	}

	public function add_city_db()
	{
		$add_city = $this->input->post('city');
		$this->count_model->create_city($add_city);
		$this->cities();
	}

	public function delete_city($id)
	{
		$this->count_model->delete_city($id);
		$this->cities();
	}


	public function edit_city($id)
	{
		$data['title'] = 'Редактирование';
		$city_data['city'] = $this->count_model->get_citys($id);
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('edit_city', $city_data);
		$this->load->view('footer');
	}

	public function update_city()
	{
		$this->load->model('count_model');
		$data=$this->input->post('city');
		$id=$this->input->post('id');
		$this->count_model->update_city($id, $data);
		header("Location: /main/cities");
	}

	public function languages()
	{
		$data['title'] = "Языки";
		$languages['lang'] = $this->count_model->get_languages();
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('languages', $languages);
		$this->load->view('footer');
	}

	public function add_languages()
	{
		$data['title'] = "Добавить язык";
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('add_language');
		$this->load->view('footer');
	}

	public function add_languages_db()
	{
		$add_languages = $this->input->post('languages');
		$this->count_model->create_languages($add_languages);
		$this->languages();
	}

	public function delete_languages($id)
	{
		$this->count_model->delete_languages($id);
		$this->languages();
	}

	public function edit_languages($id)
	{
		$data['title'] = 'Редактирование';
		$lang_data['languages'] = $this->count_model->get_lan($id);
		$this->load->view('head', $data);
		$this->load->view('menu');
		$this->load->view('edit_languages', $lang_data);
		$this->load->view('footer');
	}

	public function update_languages()
	{
		$this->load->model('count_model');
		$data=$this->input->post('languages');
		$id=$this->input->post('id');
		$this->count_model->update_languages($id, $data);
		header("Location: /main/languages");
	}

}


<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Count_model extends CI_Model 
{

  public function get_count() 
  {
      $this->db->select('*');
      $this->db->from('count');
      $query = $this->db->get();
      if ($query) 
      {
        return $query->result_array();
      }
      return false;
  }

  public function get_city($id)
  {
      $this->db->select('id, title as city_title');
      $this->db->from('city');
      $this->db->where("id_count", $id);
      $query = $this->db->get();
      if ($query) 
      {
      return $query->result_array();
      }
      return false;
  }
  
  public function get_lang($id)
  {
      $this->db->select('name');
      $this->db->from('cities_languages');
      $this->db->where("city_id", $id);
      $this->db->join('languages', 'cities_languages.language_id = languages.id');
      $query = $this->db->get();
      if ($query) 
      {
        return $query->result_array();
      }
      return false;
  }

  public function get_countries()
  {
      $this->db->select('*');
      $this->db->from('count');
      $query=$this->db->get();
      if($query)
      {
        return $query->result_array();
      }
        return false;
  }

  public function create ($add_count)
  {
      return $this->db->insert('count', $add_count);
  }

  public function delete ($id)
  {
      $this->db->where('id', $id);
      $this->db->delete('count');
  }

  public function get_counts($id) 
  {
      $this->db->select('*');
      $query = $this->db->get_where('count', array("id"=>$id));
      if ($query) 
      {
        return $query->row_array();
      }
        return false;
  }
  
  public function update($id, $data)
  {
      $this->db->where("id", $id);
      $this->db->update("count", $data);
  }

  public function get_cities()
  {
      $this->db->select('*');
      $this->db->from('city');
      $query=$this->db->get();
      if($query)
      {
        return $query->result_array();
      }
        return false;
  }

 public function create_city ($add_city)
  {
      return $this->db->insert('city', $add_city);
  }

  public function delete_city ($id)
  {
      $this->db->where('id', $id);
      $this->db->delete('city');
  }

  public function get_citys($id) 
  {
      $this->db->select('*');
      $query = $this->db->get_where('city', array("city.id"=>$id));
      if ($query) 
      {
        return $query->row_array();
      }
        return false;
  }
  
  public function update_city($id, $data)
  {
      $this->db->where("id", $id);
      $this->db->update("city", $data);
  }

  public function get_languages()
  {
      $this->db->select('*');
      $this->db->from('languages');
      $query=$this->db->get();
      if($query)
      {
        return $query->result_array();
      }
        return false;
  }

  public function create_languages ($add_languages)
  {
      return $this->db->insert('languages', $add_languages);
  }

  public function delete_languages ($id)
  {
      $this->db->where('id', $id);
      $this->db->delete('languages');
  }

  public function get_lan($id) 
  {
      $this->db->select('*');
      $query = $this->db->get_where('languages', array("id"=>$id));
      if ($query) 
      {
        return $query->row_array();
      }
        return false;
  }
  
  public function update_languages($id, $data)
  {
      $this->db->where("id", $id);
      $this->db->update("languages", $data);
  }

}

 
<form class="form-horizontal" action = "/main/add_languages_db" method = "post">

  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3">Язык *:</label>
    <div class="col-xs-9">
      <input type="text" name= "languages[name]" style="width:200px" class="form-control" placeholder="Введите язык" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>

 <div class="col-xs-offset-3 col-xs-9">
      <input type="submit" class="btn btn-primary" value="Добавить">
      <input type="reset" class="btn btn-default" value="Очистить форму">
    </div>
</form>
<form class="form-horizontal" action = "/main/add_city_db" method = "post">
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3">Город *:</label>
    <div class="col-xs-9">
      <input type="text" name= "city[title]" style="width:200px" class="form-control" placeholder="Введите город" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #9e3ea3">ID страны *:</label> 
    <div class="col-xs-9">
      <input type="text" name= "city[id_count]" style="width:200px" class="form-control" placeholder="Введите id страны" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
 <div class="col-xs-offset-3 col-xs-9">
      <input type="submit" class="btn btn-primary" value="Добавить">
      <input type="reset" class="btn btn-default" value="Очистить форму">
    </div>
</form>
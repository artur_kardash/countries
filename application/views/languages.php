<div id='langu'>
<table class="table table-hover" border="3" width="50%">
<thead>
	<tr>
		<th> ID </th>
		<th> Язык </th>
		<th> Настройки </th>
	</tr>
</thead>
<tbody>
<?php foreach ($lang as $languages): ?>
	<tr>
		<td> <?php echo $languages['id'] ?> </td>
		<td> <?php echo $languages['name'] ?> </td>
		<td> 
	         <a  class="btn btn-success" href="/main/edit_languages/<?php echo $languages['id']; ?>">Редактировать</a> 
	         <a  class="btn btn-success" href="/main/delete_languages/<?php echo $languages['id']; ?>">Удалить</a> 
		</td>
	</tr>
<?php endforeach;?>
</tbody>
</table>
	<a  class="btn btn-success" href="/main/add_languages">Добавить язык</a> 
</div>